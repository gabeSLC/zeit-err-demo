module.exports = {
	parser: 'typescript-eslint-parser',
	parserOptions: {
		jsx: true,
		useJSXTextNode: true,
	},
	extends: 'react-app',
	plugins: ['react', 'emotion'],
	rules: {
		// 'emotion/jsx-import': 'error',
		'emotion/no-vanilla': 'error',
		'emotion/import-from-emotion': 'error',
		'emotion/styled-import': 'error',
		'no-unused-vars': ['error'],
	},
};
